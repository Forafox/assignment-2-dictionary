%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define EXIT_CODE_ERROR 1 ;
%define MAX_LENGTH  255

section .rodata
    not_found_message: db 'value not found',0 
    length_error_message: db "length of the input string is greater than 255",0

section .bss
    buffer: resb 256
section .text
    global _start

_start:
    xor rax,rax
    xor rdi,rdi
    lea rsi, [buffer]
    mov rdx, MAX_LENGTH  
    syscall
    cmp rax,MAX_LENGTH            
    je .error_long_input
    lea rdi, [buffer]
    mov rsi,first_word       ; Адрес начала cловаря 
    call find_word
    test rax,rax                
    jnz .found
    mov rdi, not_found_message
    call print_error         ; Сообщения об ошибках выводятся в stderr
    mov rdi,EXIT_CODE_ERROR
    call exit

.found:
	mov rdi, rax
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi                 ; Не забываем про нуль-терминатор
    call print_string
    xor rdi,rdi             ;0 в случае успеха
    call exit
 
.error_long_input:
    mov rdi,length_error_message
    call print_error           ; Сообщения об ошибках выводятся в stderr
    mov rdi,EXIT_CODE_ERROR 
    call exit

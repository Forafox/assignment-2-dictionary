%define last_entry 0 ; Хранение указателя на последний созданный элемент в списке
%macro colon 2
    %ifidni %1, %2
        %error "Error: Duplicate label '%2'"
    %endif
    %2: dq last_entry 
    db %1, 0 
    %define last_entry %2 
%endmacro
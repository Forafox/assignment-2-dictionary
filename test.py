import subprocess
import unittest

class TestAssemblerProgram(unittest.TestCase):
    def run_assembler_program(self, input_word):
        assembler_program = "./program"
        process = subprocess.Popen(
            [assembler_program],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            shell=True
        )
        stdout, stderr = process.communicate(input=input_word)
        return process.returncode, stdout.strip(), stderr.strip()

    def test_assembler_program(self):
        word_pairs = [
            ("andrey", "first", 0, ""), 
            ("katya", "second", 0, ""),  
            ("nothing", "", 1, "value not found"), 
            ("R"*295, "", 1, "length of the input string is greater than 255"),
            ("two words", "yep", 0, "")
        ]

        for input_word, expected_output, expected_returncode, expected_stderr in word_pairs:
            with self.subTest(input_word=input_word):
                returncode, result, stderr_result = self.run_assembler_program(input_word)
                self.assertEqual(returncode, expected_returncode)
                self.assertEqual(result, expected_output)
                self.assertEqual(stderr_result, expected_stderr)

if __name__ == "__main__":
    unittest.main()

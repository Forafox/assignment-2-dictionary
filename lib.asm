%define SYS_READ        0
%define SYS_WRITE       1
%define SYS_EXIT        60

%define ASCII_SPACE     0x20
%define ASCII_TAB       0x9
%define ASCII_NEW_LINE  0xA
%define ASCII_MINUS     45

%define FD_STDOUT       1
%define FD_STDIN        0
%define FD_STDERR       2

section .data
  newLine : db ASCII_NEW_LINE, 0 ; Newline character, with 0 at the end
section .text
global exit
global string_length
global print_error
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT 
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov  rax, rdi ;
  .counter:
    cmp  byte [rdi], 0
    je   .end
    inc  rdi
    jmp  .counter
  .end:
    sub  rdi, rax
    mov  rax, rdi;
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, FD_STDERR
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov  rdx, rax; 
    mov  rax, SYS_WRITE      
    mov  rdi, FD_STDOUT
    syscall
    ret
    
; Принимает код символа и выводит его в stdout
print_char:
    push 0  ;используем стек вместо .data
    mov     rax, SYS_WRITE  ; 'write' syscall number
    mov     [rsp], rdi  ; To the reserved address
    mov     rsi, rsp    
    mov     rdi, FD_STDOUT  ; stdout descriptor
    mov     rdx, 1          ; string length in bytes
    syscall
    add rsp,8 
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ASCII_NEW_LINE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbp
    mov rbp,rsp
    sub rsp,24 ; выравниваем rsp

    mov rax, rdi        ; RAX будет использоваться для преобразования
    mov rcx, 10         ; RCX будет использоваться для деления на 10 (десятичная система)

    dec rbp 

    mov byte [rbp], 0
    .loop:
        xor rdx,rdx     ; Сброс регистра для деления
        div rcx         ; Делим RAX на RCX (т.е. на 10). 
        add dl, '0'     ; Переводим в символ 
        dec rbp 
        mov byte [rbp] , dl ;

        test rax,rax    ; Проверка на ноль
        jnz .loop
    
    after_loop:
    mov rdi, rbp
    call print_string

    add rsp, 24
    pop rbp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi,rdi
    jnl .positive
    .negative:
        push rdi
        mov rdi , ASCII_MINUS
        call print_char
        pop rdi
        neg rdi
    .positive:
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rbx ; bh+bl
    xor rax, rax ; initialize the counter (index of values in rows)
    .loop:
    mov bh, [rdi+rax] 
    mov bl, [rsi+rax] 

    cmp bh,bl 

    jne .not_equals 
    test bh, bh
    jz .equal

    inc rax
    jmp .loop

    .not_equals:
    xor rax,rax
    pop rbx
    ret
    .equal:
    mov rax, 1
    pop rbx
    ret
    
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rbp
    mov rbp,rsp
    sub rsp,24 ; выравниваем rsp

    xor rax,rax
    mov  rax, SYS_READ         ; 'read' syscall number
    mov  rdi, FD_STDIN         ; stdin descriptor
    dec rbp
    mov  rsi, rbp
    mov  rdx, 1         ; string length in bytes
    syscall

    test rax,rax
    jz .stream_is_finished
    jl .error ; если отрицательный код возврата , то переходим 

    mov al, [rbp]
    
    .stream_is_finished:
        inc rbp
    add rsp,24
    pop rbp
    ret

    .error: ; Отдельный "обработчик" ошибок. 
        inc rbp
        add rsp,24
        pop rbp
        ret

  


;Принимает: адрес начала буфера, размер буфера
;Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
;Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
;Останавливается и возвращает 0 если слово слишком большое для буфера
;При успехе возвращает адрес буфера в rax, длину слова в rdx.
;При неудаче возвращает 0 в rax
;Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    ;rdi  адрес начала буфера
    ;rsi  размер буфера
    xor rax,rax
    xor rcx,rcx         ; for string length 
    dec	rsi

    .white_space_loop:  ;cleaning up to the word
    mov r12,rcx
    mov r13,rdi
    mov r14,rsi
        call read_char 
    mov rcx,r12
    mov rdi,r13
    mov rsi,r14
    

        test al,al
        je .end 

        cmp al, ASCII_SPACE 
        je .white_space_loop
        cmp al, ASCII_NEW_LINE 
        je .white_space_loop
        cmp al, ASCII_TAB 
        je .white_space_loop
    .loop:
        cmp rcx, rsi 
        jae .buffer_overflow 

        mov byte [rdi],al
        inc rdi             ; next index +1
        inc rcx             ; word length + 1
        

    mov r12,rcx
    mov r13,rdi
    mov r14,rsi
        call read_char 
    mov rcx,r12
    mov rdi,r13
    mov rsi,r14

        test al,al
        je .end             ;если достигли конца потока
        cmp al, ASCII_SPACE 
        je .end
        cmp al, ASCII_NEW_LINE 
        je .end
        cmp al, ASCII_TAB 
        je .end

        jmp .loop
    .end:
        mov byte [rdi], 0       ;добаляем нуль-терминатор 
        mov rax, rdi             ; вернули адрес начала буфера
	sub rax, rcx
        mov rdx, rcx            ; вернули длину слова
        pop r12
        pop r13
        pop r14
        ret
    .buffer_overflow:
        xor rax,rax
        pop r12
        pop r13
        pop r14
	ret
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx

    xor rax,rax;
    xor rbx,rbx; само число
    xor r8, r8 ; кол-во символов
  
    .loop:
        movzx rbx, byte [rdi+r8]

        test rbx, rbx
        je .end 
        cmp rbx, '0'        ; Проверка, является ли символ цифрой
        jb .end   
        cmp rbx, '9'        ; Проверка, является ли символ цифрой
        ja .end    

        sub rbx, '0'        ; Преобразование символа в число
        imul rax,rax,10
        add rax, rbx
        inc r8
        jmp .loop

    .end:
        mov rdx, r8                         
        pop rbx 
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov cl , byte[rdi]
    cmp cl, ASCII_MINUS 
    je .negative

    jmp parse_uint

    .negative:
        inc rdi         ; inc string index
        call parse_uint
        neg rax
        inc rdx         ;inc string lenght
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r8, r8;

    .loop:
        cmp r8, rdx        ; current string length and buffer length
        jg .string_is_more 

        mov al, byte [rdi+r8]
        mov byte [rsi+r8], al

        test al, al
        je .finish 

        inc r8
        jmp .loop

    .string_is_more:
        xor rax,rax
        ret

    .finish:
        mov rax,r8
        ret
        

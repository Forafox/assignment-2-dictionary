%include "lib.inc"
section .text
global find_word



; . Если подходящее вхождение найдено,
;  вернёт адрес начала вхождения в  словарь (не значения), иначе вернёт 0.
find_word:
    ; RDI - указатель на нуль-терминированную строку (искомый ключ)
    ; RSI - указатель на начало словаря
    push rsi
    push r12; будет храниться адрес сравниваемой строки
    
    mov r12,rsi
    
    xor rax, rax 

    .loop:
        add r12,8           ; Пропуск места, где хранится адрес следующего звена
        mov rsi,r12 
        call string_equals
        test rax, rax
        jnz .found
        sub r12,8           ;Возращаемся на адрес следующего звена 
        mov r12, [r12]
        test r12,r12        ; проверка на конец cловаря
        jz .not_found
        jmp .loop
    .not_found:  
        pop r12
        pop rsi
        xor rax, rax        
        ret
    .found:
        mov rax, rsi        ;Возвращаем адрес начала вхождения в словарь, если найдено
    .exit:
        pop r12
        pop rsi
        ret

        
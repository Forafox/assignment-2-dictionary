ASM=nasm
ASMFLAGS=-f elf64 -g
LD=ld
PYC=python3

# Список исходных файлов
ASM_FILES=lib.asm dict.asm main.asm
# Список объектных файлов 
OBJ_FILES=$(ASM_FILES:.asm=.o)
# Правило для создания объектных файлов из ассемблерных файлов 
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm lib.inc words.inc dict.inc
dict.o: dict.asm lib.inc
words.inc: colon.inc

# Правило для создания исполняемого файла 
program: $(OBJ_FILES) 
	$(LD) -o $@ $^

.PHONY: clean
clean:
	rm -rf *.o program

.PHONY: test
test:
	make clean
	make program
	$(PYC) test.py


#Для отдельного тестирования каждого файла 
# build: main.o
# 	ld -o main main.o colon.o lib.o words.o dict.o

# main.o: main.asm dict.o words.o
# 	$(ASM) $(ASMFLAGS) -o main.o main.asm 
# lib.o: lib.asm
# 	$(ASM) $(ASMFLAGS) -o lib.o lib.asm

# dict.o: dict.asm lib.o
# 	$(ASM) $(ASMFLAGS) -o dict.o dict.asm

# words.o: words.inc colon.o 
# 	$(ASM) $(ASMFLAGS) -o words.o words.inc

# colon.o: colon.inc
# 	$(ASM) $(ASMFLAGS) -o colon.o colon.inc
